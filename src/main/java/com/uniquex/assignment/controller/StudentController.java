package com.uniquex.assignment.controller;


import com.uniquex.assignment.Exception.SystemException;
import com.uniquex.assignment.common.LogSupport;
import com.uniquex.assignment.service.StudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

@Controller
public class StudentController {

    Logger logger = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    StudentService studentService;

    /**
     * Home page
     * @param modelMap
     * @return
     */
    @GetMapping("/")
    public String home(ModelMap modelMap){
        logger.info(LogSupport.APP_HOME_PAGE,"View");
        return "home";
    }

    /**
     * sort student marks performances
     * @param file
     * @param sortingMethod
     * @param map
     * @return
     */
    @PostMapping("/sort")
    public String sort(@RequestParam("file") MultipartFile file, @RequestParam("sortingMethod") String sortingMethod,
                       ModelMap map) {
        try{
            HashMap<String, Double> sortedMap = studentService.sortStudentPerformance(file, sortingMethod);
            map.put("sortedMap", sortedMap);
            map.put("records", sortedMap.size());
        }catch (SystemException exception){
            map.put("error", exception.getMessage());
            return "error";
        }
        return "result";
    }

}
