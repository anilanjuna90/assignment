package com.uniquex.assignment.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

public interface StudentService {

    /**
     * service for sort student performance from Bubble,Merge or Hash
     * @param file
     * @param sortingMethod
     * @return HashMap<String, Double>
     */
    HashMap<String, Double> sortStudentPerformance(MultipartFile file, String sortingMethod);
}
