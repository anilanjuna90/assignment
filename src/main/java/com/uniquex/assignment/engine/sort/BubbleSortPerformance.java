package com.uniquex.assignment.engine.sort;

import com.uniquex.assignment.common.LogSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service("bubbleSortPerformance")
public class BubbleSortPerformance implements SortPerformance{

    Logger logger = LoggerFactory.getLogger(BubbleSortPerformance.class);

    @Override
    public HashMap<String, Double> sortPerformance(List<Map.Entry<String, Double>> studentPerformanceList) {
        logger.info(LogSupport.BUBBLE_SORT_STARTED);
        int i = 0, n = studentPerformanceList.size();
        boolean swapNeeded = true;
        while (i < n - 1 && swapNeeded) {
            swapNeeded = false;
            for (int j = 1; j < n - i; j++) {
                if (studentPerformanceList.get(j - 1).getValue() > studentPerformanceList.get(j).getValue()) {
                    Map.Entry<String, Double> temp = studentPerformanceList.get(j - 1);
                    studentPerformanceList.set(j - 1, studentPerformanceList.get(j));
                    studentPerformanceList.set(j, temp);
                    swapNeeded = true;
                }
            }
            if(!swapNeeded) {
                break;
            }
            i++;
        }

        // put data from sorted list to hashmap
        HashMap<String, Double> temp = new LinkedHashMap<String, Double>();
        for (Map.Entry<String, Double> aa : studentPerformanceList) {
            temp.put(aa.getKey(), aa.getValue());
        }
        logger.info(LogSupport.BUBBLE_SORT_FINISHED);
        return temp;
    }

}
