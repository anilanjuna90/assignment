package com.uniquex.assignment.engine.sort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface SortPerformance {

    HashMap<String, Double> sortPerformance(List<Map.Entry<String, Double>> studentPerformanceList);

}
