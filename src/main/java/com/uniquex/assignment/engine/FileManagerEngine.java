package com.uniquex.assignment.engine;

import com.uniquex.assignment.Exception.SystemException;
import com.uniquex.assignment.common.LogSupport;
import com.uniquex.assignment.common.Message;
import com.uniquex.assignment.helper.Document;
import com.uniquex.assignment.serviceImpl.StudentServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

@Component
public class FileManagerEngine {

    Logger logger = LoggerFactory.getLogger(FileManagerEngine.class);

    private static final String FILE_IS_NULL = "file.is.null";
    public static final String INVALID_FILE_PATH = "invalid.file.path";
    public static final String ERROR_STORE_FILE = "error.store.file";
    public static final String ERROR_CREATE_PATH = "error.create.path";


    @Autowired
    private Message messageReader;

    private final Path fileStorageLocation;

    @Autowired
    public FileManagerEngine(Document document) {
        this.fileStorageLocation = Paths.get(document.getUploadDir())
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new SystemException(messageReader.get(ERROR_CREATE_PATH), ex);
        }
    }

    /**
     * file validate and read the file then convert to Map
     *
     * @return
     */
    public Map<String, Double> getHashMapFromTextFile(MultipartFile file) {
        logger.info(LogSupport.START_VALIDATE_AND_READ_FILE);
        Map<String, Double> studentPerformancemap = null;
        if (file == null) {
            throw new SystemException(messageReader.get(FILE_IS_NULL));
        }
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new SystemException(messageReader.get(INVALID_FILE_PATH) + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            logger.info(LogSupport.TARGET_FILE_LOCATION,targetLocation.toString());
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            //Store data in file to hashmap
            studentPerformancemap = getHashMapFromTextFile(targetLocation.normalize().toString().replace("\\", "/"));

        } catch (Exception ex) {
            throw new SystemException(ERROR_STORE_FILE + fileName);
        }
        logger.info(LogSupport.END_VALIDATE_AND_READ_FILE);
        return studentPerformancemap;

    }

    public Map<String, Double> getHashMapFromTextFile(String filePath) {

        Map<String, Double> mapFileContents = new HashMap<String, Double>();
        BufferedReader br = null;
        try {
            //create file object
            File file = new File(filePath);
            //create BufferedReader object from the File
            br = new BufferedReader(new FileReader(file));
            String line = null;

            //read file line by line
            while ((line = br.readLine()) != null) {

                //split the line by :
                String[] parts = line.split(",");

                //first part is student id, second is performance
                String studentId = parts[0].trim();
                Double performance = Double.parseDouble(parts[1].trim());

                //put student id, performance in HashMap if they are not empty
                if (!studentId.equals("") && !performance.equals(""))
                    mapFileContents.put(studentId, performance);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            //Always close the BufferedReader
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                }
                ;
            }
        }

        return mapFileContents;
    }


}
