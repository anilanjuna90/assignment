package com.uniquex.assignment.serviceImpl;

import com.uniquex.assignment.Exception.SystemException;
import com.uniquex.assignment.common.Constant;
import com.uniquex.assignment.common.LogSupport;
import com.uniquex.assignment.common.Message;
import com.uniquex.assignment.controller.StudentController;
import com.uniquex.assignment.engine.FileManagerEngine;
import com.uniquex.assignment.engine.sort.SortPerformance;
import com.uniquex.assignment.service.StudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Service
public class StudentServiceImpl implements StudentService {

    Logger logger = LoggerFactory.getLogger(StudentServiceImpl.class);

    private static final String INVALID_SORTING_METHOD = "sort.method.invalid";

    @Autowired
    FileManagerEngine fileManagerEngine;

    @Autowired
    @Qualifier("bubbleSortPerformance")
    SortPerformance bubbleSortPerformance;

    @Autowired
    @Qualifier("mergeSortPerformance")
    SortPerformance mergeSortPerformance;

    @Autowired
    @Qualifier("heapSortPerformance")
    SortPerformance heapSortPerformance;

    @Autowired
    private Message messageReader;


    @Override
    public HashMap<String, Double> sortStudentPerformance(MultipartFile file, String sortingMethod) {
        logger.info(LogSupport.START_SORTING_FILE);
        HashMap<String, Double> sortedMap = null;
        try {
            Map<String, Double> studentPerformanceMap = fileManagerEngine.getHashMapFromTextFile(file);

            // Create a list from elements of HashMap
            List<Map.Entry<String, Double>> studentPerformanceList =
                    new LinkedList<Map.Entry<String, Double>>(studentPerformanceMap.entrySet());

            if (sortingMethod.equalsIgnoreCase(Constant.BUBBLE)) {
                sortedMap = bubbleSortPerformance.sortPerformance(studentPerformanceList);
            } else if (sortingMethod.equalsIgnoreCase(Constant.MERGE)) {
                sortedMap = mergeSortPerformance.sortPerformance(studentPerformanceList);
            } else if (sortingMethod.equalsIgnoreCase(Constant.HEAP)) {
                sortedMap = heapSortPerformance.sortPerformance(studentPerformanceList);
            } else {
                throw new SystemException(messageReader.get(INVALID_SORTING_METHOD));
            }
        } catch (Exception exception) {
            throw new SystemException(exception.getMessage());
        }
        logger.info(LogSupport.End_SORTING_FILE);
        return sortedMap;
    }
}
