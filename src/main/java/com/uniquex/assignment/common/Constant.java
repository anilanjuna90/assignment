package com.uniquex.assignment.common;

public class Constant {

    public static final String BUBBLE = "Bubble";
    public static final String MERGE = "Merge";
    public static final String HEAP = "Heap";

}
