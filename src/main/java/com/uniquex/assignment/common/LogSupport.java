package com.uniquex.assignment.common;

public class LogSupport {

    public static final String APP_HOME_PAGE ="App Home Page {}";
    public static final String START_SORTING_FILE ="Start sorting file";
    public static final String End_SORTING_FILE ="End sorting file";
    public static final String START_VALIDATE_AND_READ_FILE ="File validate and read start";
    public static final String END_VALIDATE_AND_READ_FILE ="File validate and read end";
    public static final String TARGET_FILE_LOCATION ="target file location is {}";
    public static final String BUBBLE_SORT_STARTED ="Start bubble sort";
    public static final String BUBBLE_SORT_FINISHED ="Finished bubble sort";

}
