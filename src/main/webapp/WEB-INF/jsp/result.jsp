<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Student Performance</title>
</head>
<body>
<h2>Sorted Student Performance</h2>

<br/>

<br/>
<table>
    <tr>

        <th>Student ID</th>
        <th>Performance</th>

    </tr>
    <c:forEach items="${sortedMap}" var="student">
        <tr>
            <td>${student.key } </td>
            <td>${student.value } </td>

        </tr>

    </c:forEach>
</table>
    <h4>No of records</h4>
    <h5>${records}</h5>
</body>
</html>
