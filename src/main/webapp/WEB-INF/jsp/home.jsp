<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>

<link rel="stylesheet" type="text/css"
	href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />

<c:url value="/css/main.css" var="jstlCss" />
<link href="${jstlCss}" rel="stylesheet" />

</head>
<body>

<form action = "sort" method = "POST" enctype="multipart/form-data">
	Attach File: <input type = "file" name = "file"/><br />
	Sorting Algorithm: <select name="sortingMethod">
	<option value="Bubble">Bubble Sort</option>
	<option value="Merge">Merge Sort</option>
	<option value="Heap">Heap Sort</option>
</select>
	<input type = "submit" value = "Submit" />

</form>
	<!-- /.container -->

	<script type="text/javascript"
		src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>

</html>
